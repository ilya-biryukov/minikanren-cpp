#include "minikanren.hpp"
#include "utility/lazy_stream.hpp"

#include <iostream>

template <class T>
void print_stream(utility::lazy_stream<T> s) {
  while (!s.empty()) {
    std::cout << s.head() << "\n";
    s.move();
  }
}

void test_fives() {
  using namespace kanren;
  using namespace utility;

  auto x = fresh<int>();
  auto y = fresh<int>();
  auto z = fresh<int>();

  std::function<auto (var<int>) -> std::function<auto (substitution) -> lazy_stream<substitution>>>
      fives = [&fives](var<int> x) {
    return disj(unify(vart(x), constt(5)),
  		[=](substitution const& s) -> lazy_stream<substitution> {
		  return bind(single(s), fives(x));
  		});
  };
  print_stream(run(call_fresh<int>(fives)));
}

void test_simple() {
  using namespace kanren;
  using namespace utility;

  auto x = fresh<int>();
  auto y = fresh<int>();
  auto z = fresh<int>();

  auto q = run(conj(unify(vart(x), vart(y)),
		    disj(unify(vart(x), constt(10)),
			 unify(vart(x), constt(20)))));
  print_stream(std::move(q));
}

void test_fives_and_sixes() {
  using namespace kanren;
  using namespace utility;


  auto x = fresh<int>();

  std::function<auto () -> std::function<auto (substitution) -> lazy_stream<substitution>>>
      fives = [&fives, x]() {
    return disj(unify(vart(x), constt(5)),
  		[=](substitution const& s) -> lazy_stream<substitution> {
		  return bind(single(s), fives());
  		});
  };
  std::function<auto () -> std::function<auto (substitution) -> lazy_stream<substitution>>>
      sixes = [&sixes, x]() {
    return disj(unify(vart(x), constt(6)),
  		[=](substitution const& s) -> lazy_stream<substitution> {
		  return bind(single(s), sixes());
  		});
  };
  print_stream(run(disj(fives(), sixes())));
}

int main() {
  // test_simple();
  // test_fives();
  test_fives_and_sixes(); // doesn't work yet
}
