#include <cassert>
#include <memory>
#include <utility>
#include "utility/optional.hpp"

namespace utility {
  template <class T>
  struct list {
  private:
    struct node;
    using node_ptr = std::shared_ptr<node>;

    struct node {
      node(T head, node_ptr tail)
	  : head(head)
	  , tail(tail) {
      }

      T head;
      std::shared_ptr<node> tail;
    };

    explicit list(node_ptr node)
	: node_(node) {
    }

  public:
    list() = default;

    bool empty() const {
      return !node_;
    }

    list prepend(T val) const {
      return list(std::make_shared<node>(std::move(val), node_));
    }

    T const& head() const {
      assert(node_ && "head is called for empty list");
      return node_->head;
    }

    list tail() const {
      assert(node_ && "tail is called for empty list");
      return list(node_->tail);
    }

  private:
    node_ptr node_ = nullptr;
  };

  template <class K, class V>
  struct assoc_list {
  private:
    explicit assoc_list(list<std::pair<K, V>> lst)
	: lst_(lst) {
    }

  public:
    assoc_list() = default;
    
    V const* find(K const& k) const {
      for (auto it = lst_; !it.empty(); it = it.tail()) {
	auto&& kv = it.head();
	if (kv.first == k)
	  return &kv.second;
      }

      return nullptr;
    }

    assoc_list add(K k, V v) const {
      return assoc_list(lst_.prepend(std::make_pair(k, v)));
    }

    list<std::pair<K, V>> get_all_values() const {
      return lst_;
    }

  private:
    list<std::pair<K, V>> lst_;
  };
}
