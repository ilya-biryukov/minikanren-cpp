#pragma once

#ifdef __GNUC__

#define unreacahble() __builtin_unreachable()

#else

#include <cassert>
#define unreachable() assert(false && "should not be reached")

#endif 
