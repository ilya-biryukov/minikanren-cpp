#pragma once

#include <eggs/variant.hpp>
#include <functional>
#include <type_traits>
#include "utility/overloads.hpp"
#include "utility/optional.hpp"

namespace utility {
  using eggs::variant;

  template <class Ret>
  struct move_function_impl {
    virtual ~move_function_impl() = default;

    virtual Ret apply() = 0;
  };

  template <class Ret, class Func>
  struct move_function_object_impl : move_function_impl<Ret> {
    static_assert(!std::is_reference<Func>::value, "should not be a reference");

    move_function_object_impl(Func&& f)
	: f_(std::move(f)){
    }

    Ret apply() override {
      return f_();
    }

  private:
    Func f_;
  };
  
  template <class T>
  struct move_function;

  template <class Ret>
  struct move_function<Ret()> {
    template <class Func>
    explicit move_function(Func&& f)
	: impl_(std::make_unique<move_function_object_impl<Ret, std::remove_reference_t<Func>>>(std::move(f))) {
    }

    Ret operator()() const {
      return impl_->apply();
    }
  private:
    std::unique_ptr<move_function_impl<Ret>> impl_;
  };

  template <class T>
  struct deferred {
    deferred(deferred const&) = delete;
    deferred(deferred &&) = default;

    deferred& operator = (deferred const&) = delete;
    deferred& operator = (deferred &&) = default;

  public:
    template <class Func>
    static deferred<T> create_deferred(Func&& f) {
      return deferred<T>(move_function<T()>(std::move(f)));
    }

    template <class U>
    static deferred<T> create_forced(U&& u) {
      return deferred<T>(T(std::move(u)));
    }

  private:
    explicit deferred(eggs::variant<move_function<T()>, T> v)
	: val_(std::move(v)) {
    }

  public:
    template <class U>
    deferred(deferred<U>&& other)
	: val_(std::move(other.val_)) {
    }

    template <class U>
    deferred& operator=(deferred<U>&& other) {
      val_ = std::move(other.val_);
      return *this;
    }

    T const& get() const {
      force();
      return eggs::variants::get<T>(val_);
    }

    T& get() {
      force();
      return eggs::variants::get<T>(val_);
    }

  private:
    void force() const {
      if (auto fn = val_.template target<move_function<T()>>())
	val_ = (*fn)();
    }

  private:
    template <class>
    friend struct deferred;

  private:
    mutable eggs::variant<move_function<T()>, T> val_;
  };

  template <class T>
  deferred<T> defer(move_function<T()> f) {
    return deferred<T>::create_deferred(f);
  }

  template <class Func>
  auto defer(Func&& f) {
    return defer(move_function<decltype(f())()>(std::forward<Func>(f)));
  }

  template <class T>
  deferred<T> hurry(T t) {
    return deferred<T>::create_forced(std::move(t));
  }

  template <class T>
  struct lazy_stream {
    using res = utility::optional<std::pair<T, std::unique_ptr<lazy_stream>>>;
    
  public:
    explicit lazy_stream(deferred<res> fn)
      : val_(std::move(fn)) {
    }

    bool empty() const {
      return !val_.get();
    }

    T const& head() const {
      return (*val_.get()).first;
    }

    void move() {
      auto&& next_stream = (*val_.get()).second;
      if (next_stream)
      {
	deferred<res> new_val = std::move(next_stream->val_);
	val_ = std::move(new_val);
      }
      else
      {
	val_ = hurry(res(utility::nullopt));
      }
    }

    res deconstruct() {
      return std::move(val_.get());
    }
		
  private:
    deferred<res> val_;
  };

  template <class T>
  lazy_stream<T> empty() {
    return lazy_stream<T>(hurry(typename lazy_stream<T>::res()));
  }

  template <class T>
  lazy_stream<T> single(T e) {
    return lazy_stream<T>(hurry(utility::make_optional(std::make_pair(std::move(e), std::unique_ptr<lazy_stream<T>>(nullptr)))));
  }

  template <class T, class Func>
  lazy_stream<T> from_producer(Func producer) {
    return lazy_stream<T>(defer([producer=std::move(producer)]() mutable -> typename lazy_stream<T>::res {
      utility::optional<T> item = producer();
      if (!item)
	return utility::nullopt;
      return utility::make_optional(std::make_pair(
	std::move(*item),
	std::make_unique<lazy_stream<T>>(from_producer<T>(std::move(producer)))));
    }));
  }

  template <class T>
  lazy_stream<T> concat(lazy_stream<T> lhs, lazy_stream<T> rhs) {
    return lazy_stream<T>(defer([lhs = std::move(lhs), rhs = std::move(rhs)]() mutable -> typename lazy_stream<T>::res {
      utility::optional<T> head = utility::nullopt;
      if (!lhs.empty()) {
	head = lhs.head();
	lhs.move();
      } else if (!rhs.empty()) {
	head = rhs.head();
	rhs.move();
      }

      if (!head)
	return utility::nullopt;
      return utility::make_optional(std::make_pair(
	  std::move(*head),
	  std::make_unique<lazy_stream<T>>(concat(std::move(lhs), std::move(rhs)))));
    }));
  }

  template <class T, class Func>
  lazy_stream<T> bind(lazy_stream<T> lhs, Func f) {
    return lazy_stream<T>(defer([lhs = std::move(lhs), f = std::move(f)]() mutable -> typename lazy_stream<T>::res {
      if (!lhs.empty()) {
	T head = lhs.head();
	lhs.move();
	return concat(f(head), bind(std::move(lhs), f)).deconstruct();
      }

      return utility::nullopt;
    }));
  }
} // namespace utility
