#pragma once

#include <experimental/optional>

namespace utility {
  using std::experimental::optional;
  using std::experimental::nullopt;
  using std::experimental::make_optional;
} // namespace utility
