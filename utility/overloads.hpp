#pragma once

namespace utility {
  namespace detail {
    template <class F>
    struct single_overload : F {
      single_overload(F&& f)
	: F(std::forward<F>(f)) {
      }

      using F::operator();
    };

    template <class ...Fs>
    struct many_overloads : single_overload<Fs>... {
      many_overloads(Fs&& ...fs)
	: single_overload<Fs>(std::forward<Fs>(fs))...{
      }
    };
  } // namespace detail

  template <class ...Fs>
  auto overloads(Fs&& ...fs) {
    return detail::many_overloads<Fs...>(std::forward<Fs>(fs)...);
  }
} // namespace utility
