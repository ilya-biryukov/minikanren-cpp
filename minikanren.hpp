#pragma once

#include <cstddef>
#include <unordered_map>
#include <memory>
#include <cassert>
#include <ostream>

#include "utility/overloads.hpp"
#include "utility/unreachable.hpp"
#include "utility/list.hpp"
#include "utility/lazy_stream.hpp"


namespace kanren {

using utility::overloads;
using utility::lazy_stream;
using utility::empty;
using utility::single;

template <class T>
struct var {
  static var make() {
    static size_t index = 0;
    return var(index++);
  }

private:
  var(size_t index) : index_(index) {
  }

public:
  std::size_t get_index() const {
    return index_;
  }

private:
  std::size_t index_;
};

template <class T>
var<T> fresh(){
  return var<T>::make();
}

struct some_term {
  virtual std::ostream& print_term(std::ostream& s) = 0;

  virtual ~some_term() = 0;
};

some_term::~some_term() = default;

template <class T>
struct term : some_term {
  virtual ~term() = 0;
};

template <class T>
term<T>::~term() = default;

template <class T>
struct var_term : term<T> {
  var_term(var<T> v) : v_(v) {}

  var<T> get_var() const {
    return v_;
  }

  std::ostream& print_term(std::ostream& s) override {
    return s << *this;
  }

private:
 var<T> v_;
};


template <class T>
struct value_term : term<T> {
  value_term(T value) : value_(value) {
  }

  T const& get_value() const {
    return value_;
  }

  std::ostream& print_term(std::ostream& s) override {
    return s << *this;
  }

private:
  T value_;
};

template <class T>
using term_ptr = std::shared_ptr<term<T>>;

template <class T, class Func>
auto visit_term(term_ptr<T> term,  Func f) {
  assert(term && "term is null");
  if (auto vt = std::dynamic_pointer_cast<var_term<T>>(term))
    return f(vt);
  if (auto vt = std::dynamic_pointer_cast<value_term<T>>(term))
    return f(vt);
  unreacahble();
}

struct substitution {
  using mapping = utility::assoc_list<int, std::shared_ptr<some_term>>;

public:
  substitution() = default;

private:
  substitution(mapping subst)
      : subst_(subst) {
  }

public:
  template <class T>
  substitution add(var<T> v, std::shared_ptr<term<T>> val) const {
    return subst_.add(v.get_index(), val);
  }

  template<class T>
  term_ptr<T> get(var<T> val) const {
    auto it = subst_.find(val.get_index());
    if (!it)
      return nullptr;
    return std::static_pointer_cast<term<T>>(*it);
  }

  mapping get_all_substitutions() const {
    return subst_;
  }

private:
  mapping subst_;
};

template <class T>
term_ptr<T> walk(term_ptr<T> v, substitution const& s) {
  assert(v && "v should not be null");
  return visit_term(v, overloads(
    [&](std::shared_ptr<var_term<T>> var)-> term_ptr<T> {
      auto u = s.get(var->get_var());
      if (!u)
	return var;
      return walk(u, s);
    },
    [&](std::shared_ptr<value_term<T>> v) -> term_ptr<T> {
      // TODO: learn to walk the value terms
      return v;
    }));
};

template <class T>
auto unify(term_ptr<T> lhs, term_ptr<T> rhs) {
  return [lhs=std::move(lhs), rhs=std::move(rhs)](substitution const& s) mutable -> lazy_stream<substitution> {
    lhs = walk(lhs, s);
    rhs = walk(rhs, s);

    if (!dynamic_cast<var_term<T>*>(lhs.get()))
      swap(lhs, rhs);

    return visit_term(lhs, overloads(
      [&](std::shared_ptr<var_term<T>> vl) {
	return single(s.add(vl->get_var(), rhs));
      },
      [&](std::shared_ptr<value_term<T>> vl) {
	auto vr = dynamic_cast<value_term<T>*>(rhs.get());
	if (!vr || vl->get_value() != vr->get_value())
	  return utility::empty<substitution>();
	return single(s);
      }));
  };
}

template <class Lhs, class Rhs>
auto conj(Lhs lhs, Rhs rhs) {
  return [lhs=std::move(lhs), rhs=std::move(rhs)](substitution const& s) mutable -> lazy_stream<substitution> {
    return bind(lhs(s), rhs);
  };
}

template <class Lhs, class Rhs>
auto disj(Lhs lhs, Rhs rhs) {
  return [lhs=std::move(lhs), rhs=std::move(rhs)](substitution const& s) mutable -> lazy_stream<substitution> {
    return concat(lhs(s), rhs(s));
  };
}

template <class T, class Func>
auto call_fresh(Func f) {
  return f(fresh<T>());
}

template <class Goal>
lazy_stream<substitution> run(Goal g) {
  return g(substitution());
}


template <class T>
term_ptr<T> vart(var<T> v) {
  return std::make_shared<var_term<T>>(v);
}

template <class T>
term_ptr<T> constt(T v) {
  return std::make_shared<value_term<T>>(v);
}

template <class T>
std::ostream& operator << (std::ostream& s, var<T> const& var) {
  return s << "_." << var.get_index();
}

template <class T>
std::ostream& operator << (std::ostream& s, var_term<T> const& term) {
  return s << term.get_var();
}
template <class T>
std::ostream& operator << (std::ostream& s, value_term<T> const& term) {
  return s << term.get_value();
}

std::ostream& operator << (std::ostream& s, std::shared_ptr<some_term> const& term) {
  return term->print_term(s);
}

std::ostream& operator << (std::ostream& s, substitution const& subst) {
  bool first = true;
  s << "{";
  for (auto lst = subst.get_all_substitutions().get_all_values(); !lst.empty(); lst = lst.tail()) {
    if (!first) {
      s << ", ";
    } else {
      first = false;
    }

    auto&& p = lst.head();
    s << p.first << ": " << p.second;
  }
  return s << "}";
}

} // namespace kanren
